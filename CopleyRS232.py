import time
import serial
from datetime import datetime

class CopleyRS232:
    
    nodeID = ""
    axisLetter = ""
    defaultBaud = 9600
    defaultTimeout = .1
    defaultPortID = '/dev/ttyUSB1' 
    
    #parameter dictionary and values
    
    #position parameters
    param_commanded_position_ID = '0x3d'
    param_motor_position_ID = '0x32'
    param_actual_position_ID = '0x17'
    param_load_position_ID = '0x112'
    param_limited_position_ID = '0x2d'
    param_trajectory_mode_ID = '0x24'
    param_following_error_ID = '0x35'

    # trajectory mode values  
    param_trajectory_mode_velocity = 11
    param_trajectory_mode_position = 21
    param_trajectory_mode_disable = 0

    #homing
    param_home_offset_ID = '0xc6'
    
    #trajectories
    param_trajectory_stop = 0
    param_trajectory_move = 1
    param_trajectory_home = 2

    #Programmed position mode params
    param_profile_ID = "0xc8"
    param_profile_absTrapMove = 0
    param_profile_absSMove = 1
    param_profile_veloMove = 2
    param_profile_relTrapMove = 256
    param_profile_relSMove = 257
    param_position_command_ID = "0xca"
    param_position_mode_posVelo = 1
    param_position_mode_negVelo = -1
    param_maxVelo_ID = "0xcb"
    param_maxAccel_ID = "0xcc"
    param_maxDecel_ID = "0xcd"
    param_maxJerk_ID = "0xce"
    param_abortDecel_ID = "0xcf"
    param_commanded_current_ID = "0x15"
    
    #in/out
    param_analogInVoltage_ID = "0x1d"
    param_inputs_ID = "0xa6"
    param_outputs_ID = "0xab"
    
    #faults/states
    param_statusRegister_ID = "0xa0"
    param_trajectoryRegister_ID = "0xc9"
    param_faultRegister_ID = "0xa4"
    
    #misc
    param_busVoltage_ID = "0x1E"
    param_ampTemp_ID = "0x20"
    param_baud_ID = "0x90"
    
    # memories
    ram = 'r'
    flash = 'f'
    
    # params for direct current control (dumb motors, coils, magnets, etc)
    param_current_program = "0x02"
    param_actual_current_readback_ID = "0x38"
    param_current_ramp_rate = "0x6a"    
    velocity_move_active = False

    # bytestream to allow/disallow CVM
    enableCVM = bytearray([0x00,0x45,0x02,0x14,0x00,0x09,0x00,0x00]) 
    disableCVM = bytearray([0x00,0x45,0x01,0x14,0x00,0x0a])   

    # Init object
    def __init__(self):
        pass 
    
    # Open serial object
    def StartSerial(self,baud=defaultBaud,serialPort=defaultPortID,timeout=defaultTimeout,nodeID=nodeID,axisLetter=axisLetter):
        
        self.CopleySerial = serial.Serial()
        self.CopleySerial.timeout=timeout
        self.CopleySerial.port=serialPort
        self.CopleySerial.baudrate=9600
        self.CopleySerial.open()
        time.sleep(1)
        self.ChangeSerialSpeed(baud)
        self.axisLetter = axisLetter
        self.nodeID = nodeID


        return self.CopleySerial
    
    # Close serial object
    def StopSerial(self):

        self.CopleySerial.close()

    # Change the serial speed
    def ChangeSerialSpeed(self,baud):
        
        self.CopleySerial.read_all()
        self.CopleySerial.sendBreak()
        time.sleep(1)
        self.CopleySerial.write(('g r'+str(self.param_baud_ID) + '\r').encode())
        self.CopleySerial.read_all()
        commandstring = 's r'+str(self.param_baud_ID)+' '+str(int(baud)) + '\r'
        self.CopleySerial.write(commandstring.encode())
        #self.CopleySerial.close()
        time.sleep(1)
        self.CopleySerial.baudrate=baud
        time.sleep(1)
        self.CopleySerial.read_all()
        self.CopleySerial.write(('g r'+str(self.param_baud_ID) + '\r').encode())
        time.sleep(1)
        self.CopleySerial.read_all()
        #self.CopleySerial.open()
    
    # Command structure
    def CopleyCommand(self,nodeID="",axisLetter="",commandCode="",commandParameters=""):
        
        commandString = str(nodeID) + str(axisLetter) + '' + str(commandCode) + str(commandParameters).strip() +'\r' 
        self.CopleySerial.write(commandString.encode())
        response = self.CopleySerial.read_until('\r'.encode()).decode()
        response = response[0:(len(response)-1)]

        return response

    # Set Command
    def Set(self,memoryBank,parameterID,value):
        
        return self.CopleyCommand(self.nodeID, self.axisLetter,'s ',(str(memoryBank) + str(parameterID) + ' ' + str(value)))
        
    # Get Command
    def Get(self,memoryBank,parameterID,optional=""):
        
        if optional!="":
            return self.CopleyCommand(self.nodeID, self.axisLetter,'g ',(str(memoryBank) + str(parameterID) + ' ' + str(optional)))
        else:
            return self.CopleyCommand(self.nodeID, self.axisLetter,'g ',(str(memoryBank) + str(parameterID)))
    
    def Reset(self):
    
        return self.CopleyCommand("","","r","")
        
    def Trajectory(self,commandCode):
    
        return self.CopleyCommand(self.nodeID, self.axisLetter,'t ',commandCode)
        
    def ClearEncoderErrors(self,encoder="both"):
        
        if encoder == "load":
            self.CopleySerial.write("ldenc clear\r".encode())
        if encoder == "motor":
            self.CopleySerial.write("enc clear\r".encode())
        else:
            self.CopleySerial.write("enc clear\r".encode())
            self.CopleySerial.write("ldenc clear\r".encode())
        
        return "ok"

    def FlushSerial(self):
        
        #grabs all waiting data
        response = self.CopleySerial.read_all()
        
        return response

    def VelocityMove(self,velocity_rotations_per_second,accel_counts_per_second_squared=-1,decel_counts_per_second_squared=-1):

        # sets the amp velocity, and enables places the in velocity mode (if it isn't already)
        self.SetVelocityParameter(velocity_rotations_per_second,allowNegative=True)
        self.SetAccelParameter(accel_counts_per_second_squared)
        self.SetDecelParameter(decel_counts_per_second_squared)
        self.velocity_move_active = True
        self.EnableVelocityMode()

        return velocity_rotations_per_second

    def PositionVelocityMove(self,velocity_rotations_per_second,accel_counts_per_second_squared=-1,decel_counts_per_second_squared=-1,jerk=-1):
        
        # places the amp in the velocity position move mode.
        # velocity move mode uses the velocity_rotations_per_second as the final velocity, a position of 1 for positive direction, and a position of -1 for negative direction.
        # Note that positive and negative are defined by the amp and don't directly relate to, say, CW or CCW.

        velocityValid = False
        
        if velocity_rotations_per_second>0.1:
            self.Set(self.ram,self.param_position_command_ID,1)
            velocityValid = True
        elif velocity_rotations_per_second<-0.1:
            self.Set(self.ram,self.param_position_command_ID,-1) 
            velocity_rotations_per_second = abs(velocity_rotations_per_second)
            velocityValid = True
        # otherwise, velocity is NOT valid. If not valid, don't run the move.
        
        if velocityValid:

            self.SetVelocityParameter(velocity_rotations_per_second)
            self.SetAccelParameter(accel_counts_per_second_squared)
            self.SetDecelParameter(decel_counts_per_second_squared)

            self.Set(self.ram,self.param_profile_ID,self.param_profile_veloMove)

            return self.StartPositionMove()
        
        else:
            return -1

    def StopMove(self):

        # handle the velocity mode case.
        if self.velocity_move_active:
            self.SetVelocityParameter(0)
            self.Disable()
        
        self.Trajectory(self.param_trajectory_stop)
    
    def AbsolutePositionMove(self,position,velocity_counts_per_second=1500,accel_counts_per_second_squared=11,decel_counts_per_second_squared=11,jerk=160):
        
        # if any of the velocity/accel/decel/jerk parameters are present and valid, save them.
        self.SetVelocityParameter(velocity_counts_per_second)
        self.SetAccelParameter(accel_counts_per_second_squared)
        self.SetDecelParameter(decel_counts_per_second_squared)

        # define the move params.
        self.Set(self.ram,self.param_profile_ID,self.param_profile_absSMove)
        self.Set(self.ram,self.param_position_command_ID,int(position))
        
        # start the move and return the trajectory generator code.
        code = self.StartPositionMove()
        print(code)
        return code        

    def RelativePositionMove(self,position,velocity_counts_per_second=-1,accel_counts_per_second_squared=-1,decel_counts_per_second_squared=-1,jerk=-1):

        # if any of the velocity/accel/decel/jerk parameters are present and valid, save them.
        self.SetVelocityParameter(velocity_counts_per_second)
        self.SetAccelParameter(accel_counts_per_second_squared)
        self.SetDecelParameter(decel_counts_per_second_squared)

        # define the move params.
        self.Set(self.ram,self.param_profile_ID,self.param_profile_relSMove)
        self.Set(self.ram,self.param_position_command_ID,int(position))
        
        # start the move and return the trajectory generator code.
        return self.StartPositionMove()        

    def Home(self):
        self.Disable()
        time.sleep(.01)
        self.EnablePositionMode()
        time.sleep(.01)
        self.Trajectory(self.param_trajectory_home)
        time.sleep(.01)

        pass

    def SetVelocityParameter(self,velocity_counts_per_second,allowNegative=False):

        if allowNegative:
            
            # amp uses units of 1 = .1 counts/second.
            velocity_tenth_counts_per_second = int(velocity_counts_per_second*10)
            self.Set(self.ram,self.param_maxVelo_ID,velocity_tenth_counts_per_second)
        
        elif velocity_counts_per_second>0:
            
            # amp uses units of 1 = .1 counts/second.
            velocity_tenth_counts_per_second = int(abs(velocity_counts_per_second*10))
            self.Set(self.ram,self.param_maxVelo_ID,velocity_tenth_counts_per_second)

    def SetAccelParameter(self,accel_counts_per_second_squared):

        if accel_counts_per_second_squared>=10:
            # amp uses units of 1 = 10 counts/second^2.
            accel_counts_per_second_squared = int(accel_counts_per_second_squared/10)
            self.Set(self.ram,self.param_maxAccel_ID,accel_counts_per_second_squared)

    def SetDecelParameter(self,decel_counts_per_second_squared):
   
        if decel_counts_per_second_squared>=10:

            # amp uses units of 1 = 10 counts/second^2.
            decel_counts_per_second_squared = int(decel_counts_per_second_squared/10)
            self.Set(self.ram,self.param_maxDecel_ID,decel_counts_per_second_squared)
            self.Set(self.ram,self.param_abortDecel_ID,decel_counts_per_second_squared)

    def SetJerkParameter(self,jerk):

        if jerk>=100:

            # amp uses units of 1 = 100 counts/second^3.
            jerk = int(jerk/10)
            self.Set(self.ram,self.param_maxJerk_ID,jerk)

    def StartPositionMove(self):
        
        # Starts position move.
        self.EnablePositionMode()
        time.sleep(.01)
        print(str(datetime.now())+": Starting Move")
        response = self.Trajectory(self.param_trajectory_move)
        print(response)
        return response
    
    def TrajectoryMode(self,mode=param_trajectory_mode_position):

        self.Set(self.ram,self.param_trajectory_mode_ID,mode)

    def GetInputs(self):
            
        response = self.Get(self.ram,self.param_inputs_ID)
        # remove the lead characters in response.
        return self.StripGetPrefix(response)

    def GetOutputs(self):
        
        response = self.Get(self.ram,self.param_outputs_ID)
        return self.StripGetPrefix(response) 

    def GetActualPosition(self):
            
        response = self.Get(self.ram,self.param_actual_position_ID)
        # remove the lead characters in response.

        return self.StripGetPrefix(response)

    def GetCommandedCurrent(self):
        response = self.Get(self.ram,self.param_commanded_current_ID)

        return self.StripGetPrefix(response)/100

    def GetActualCurrent(self):
        response = self.Get(self.ram,self.param_actual_current_readback_ID)

        return self.StripGetPrefix(response)/100
    def GetLoadPosition(self):
            
        response = self.Get(self.ram,self.param_load_position_ID)
        # remove the lead characters in response.

        return self.StripGetPrefix(response)

    def GetMotorPosition(self):
        
        response = self.Get(self.ram,self.param_limited_position_ID)
        return self.StripGetPrefix(response)
    
    def GetFollowingError(self):
        
        response = self.Get(self.ram,self.param_following_error_ID)
        return self.StripGetPrefix(response)

    def GetStatusRegister(self):

        response = self.Get(self.ram,self.param_statusRegister_ID)
        
        return self.StripGetPrefix(response) 
        
    def GetLatchingFaultRegister(self):

        response = self.Get(self.ram,self.param_faultRegister_ID)
        return self.StripGetPrefix(response)
        
    def GetTrajectoryRegister(self):
        
        response = self.Get(self.ram,self.param_trajectoryRegister_ID)
        return self.StripGetPrefix(response)

    def SetOutputs(self,outputs):
        print("setting outputs: " + str(int(outputs)))
        self.Set(self.ram,self.param_outputs_ID,int(outputs))

    def StripGetPrefix(self,response):
                        
        return int(response[2:len(response)])

    def ClearError(self):
        # write a 1 to bits 0-12.
        self.Set(self.ram,self.param_faultRegister_ID,8191)

    def Disable(self):

        self.Set(self.ram,self.param_trajectory_mode_ID,self.param_trajectory_mode_disable)

    def EnableVelocityMode(self):

        self.Set(self.ram,self.param_trajectory_mode_ID,self.param_trajectory_mode_velocity)

    def EnablePositionMode(self):

        self.Set(self.ram,self.param_trajectory_mode_ID,self.param_trajectory_mode_position)

    def GetAnalog(self):

        return self.Get(self.ram,self.param_analogInVoltage_ID)

    def GetRegister(self,register = 0):
        # gets a register
        response = self.StripGetPrefix(self.CopleyCommand(self.nodeID, self.axisLetter,'i r',(str(int(register)))))

        #print(response)

        return response

    def SetRegister(self,value,register = 0):
        response = self.CopleyCommand(self.nodeID, self.axisLetter,'i r',(str(int(register)))+' '+ str(int(value)))
        print(response)

        return response

    def EnableIndexer(self):
        # enables the indexer via bytecommand.
        
        self.CopleySerial.write(self.enableCVM)
        print(str(datetime.now())+ ": INDEXER ENABLED")
        time.sleep(.01)
        return self.CopleySerial.read_all()

    def DisableIndexer(self):
        # enables the indexer via bytecommand.
        self.CopleySerial.write(self.disableCVM)
        time.sleep(.01)
        return self.CopleySerial.read_all()
