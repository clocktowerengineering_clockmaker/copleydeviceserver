class CopleyErrorDecoder():


    # defines an error decoder ring for Copley amplifiers.
    
    string_bit_separator = ", "
    string_line_terminator = "/r/n"
    

    event_status_dict = [ "Short circuit detected",
                        "Drive over temperature",
                        "Over voltage",
                        "Under voltage",
                        "Motor temperature sensor active",
                        "Feedback error or Encoder power error",
                        "Motor phasing error",
                        "Current output limited",
                        "Voltage output limited",
                        "Positive limit switch active",
                        "Negative limit switch active",
                        "Enable input not active",
                        "Drive is disabled by software (desired state is 0)",
                        "Trying to stop motor",
                        "Motor brake activated",
                        "PWM outputs disabled",
                        "Positive software limit condition",
                        "Negative software limit condition",
                        "Tracking (Following) Error Fault. A tracking (following) error has occurred, and drive is in tracking (following) error mode.",
                        "Tracking (Following) Error Warning. Indicates position error is greater than position tracking (following) warning.",
                        "Drive is currently in reset condition",
                        "Position has wrapped. Position variable cannot increase indefinitely.",
                        "Drive fault. Fault configured as latching in Fault Mask (0xA7) has occurred. Latched faults may be cleared using Latching Fault Status Register (0xA4).",
                        "Velocity limit (0x3A) has been reached",
                        "Acceleration limit (0x36) has been reached",
                        "Position Tracking. Position Loop Error (0x35) is outside of Tracking (Following Error Fault Limit (0xBA).",
                        "Home switch is active",
                        "In motion. Bit is set if trajectory generator is running profile or Tracking (Following Error Fault Limit (0xBA) is outside tracking window. Clear when drive is settled in position.",
                        "Velocity window. Set when velocity error is larger than programmed velocity window",
                        "Phase not yet initialized. This bit is set until drive has initialized its phase. Drive is performing algorithmic phasing, or phase initialization has failed.",
                        "Command fault. CANopen or EtherCAT master not sending commands in time as configured by the master, or PWM command not present.",
                        "Reserved."
                        ]
    
    latching_fault_dict = [ "Data flash CRC failure.",
                            "A/D offset out of range (fatal fault). Drive internal error. ",
                            "Short circuit. ",
                            "Drive over temperature. ",
                            "Motor over temperature.",
                            "Over-voltage.",
                            "Under-voltage. ",
                            "Feedback fault.  Feedback faults occur if too much current is drawn from 5 V source on drive, resolver or analog encoder is disconnected, or resolver or analog encoder has levels out of tolerance.",
                            "Phasing error.",
                            "Following error.",
                            "I2T Error. Output current is limited by I2T algorithm.",
                            "FPGA failure.",
                            "Command input lost fault.",
                            "Unable to initialize internal drive hardware. This bit is read-only.",
                            "Safety circuit consistency check failure.",
                            "Drive is unable to control motor current.",
                            "Motor wiring is disconnected, see Open Motor Wiring Check Current (0x19D).",
                            "Reserved.",
                            "Safe torque off active."
                            ]
    
    def CheckIntForBitPosition(self,variable,bit):

        # returns whether the incoming integer has a binary true at the indicated binary position, 0 aligned.    
        response = (int(variable) & int(2**(bit))) > (bit)
        return response
    

    def BuildString(self,word,dictionary_to_use):

# builds the string based on the value coming through. The incoming int gets checked bitwise, and anything true gets stuffed into an output string. String separator and terminator are class var.
        
        output_string = ""
        for index_variable in range(len(dictionary_to_use)):

            bit_result = self.CheckIntForBitPosition(word,index_variable)
            if bit_result:

                output_string += dictionary_to_use[index_variable]
                output_string += self.string_bit_separator
            
        output_string += self.string_line_terminator
        return output_string

    def BuildEventString(self,eventword):

        # convenience method for the event dictionary
        return self.BuildString(eventword,self.event_status_dict)


    def BuildLatchingString(self,eventword):

        # convenience method for the event dictionary
        return self.BuildString(eventword,self.latching_fault_dict)
